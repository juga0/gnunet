#!/bin/bash

set -x

# When running the test in anther docker container (like Gitlab CI, the path
# must has to be inside the project root dir.)
# Call from the project root dir as contrib/docker/test.sh
mkdir -p ./tmp/gnunet1/.config
mkdir -p ./tmp/gnunet2/.config
chown -R 1000:1000 ./tmp/gnunet1 ./tmp/gnunet2

docker-compose -f ./contrib/docker/docker-compose.yml up -d
sleep 3

echo "Testing friends."
echo "Obtain gnunet1 hello"
HELLO1=$(docker exec gnunet1 gnunet-peerinfo -gn)
echo $HELLO1
echo "Add gnunet1 as a friend in gnunet2"
docker exec gnunet2 gnunet-peerinfo -p $HELLO1

echo "Obtain gnunet2 hello string"
HELLO2=$(docker exec gnunet2 gnunet-peerinfo -gn)
echo $HELLO2
echo "Add gnunet2 as a friend in gnunet1"
docker exec gnunet1 gnunet-peerinfo -p $HELLO2


echo "Testing GNS."
# In gnunet1
echo "In gnunet1, create an identity"
docker exec gnunet1 gnunet-identity -C myself
echo "Check it and store it to use it in gnunet2"
GNUNET1=$(docker exec gnunet1 gnunet-identity -d | grep myself | awk '{print $3}')
echo $GNUNET1

echo "gnunet1 adds a public A record to the zone"
docker exec gnunet1 gnunet-namestore -z myself -a -e "1 d" -p -t A -n ccc -V 195.54.164.39
echo "Query the A record"
docker exec gnunet1 gnunet-gns -t A -u ccc.myself
echo "Display records in gnunet1 zone myself"
docker exec gnunet1 gnunet-namestore -z myself -D

# In gnunet2
echo "In gnunet2, create an identity"
docker exec gnunet2 gnunet-identity -C bob
echo "Delegate records in gnunet1 to gnunet2"
# When using gnunet version > v0.13.3, this returns:
# ERROR Record type does not match parsed record type
docker exec gnunet2 gnunet-namestore -z bob -a -e never -p -t PKEY -n gnunet1 -V $GNUNET1
echo "Check gnunet1 records can be solved in gnunet2"
# Line commented cause takes forever
# docker exec gnunet2 gnunet-gns -t A -u ccc.gnunet1.bob
