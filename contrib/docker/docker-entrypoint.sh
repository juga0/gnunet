#!/bin/bash

set -x

sed -i 's/$GNUNET_PORT/'${GNUNET_PORT:-2086}'/g'  /home/gnunet/.config/gnunet.conf

if [[ $# -eq 0 ]]; then
  exec gnunet-arm \
    --start \
    --monitor
elif [[ -z $1 ]] || [[ ${1:0:1} == '-' ]]; then
  exec gnunet-arm "$@"
else
  exec "$@"
fi
